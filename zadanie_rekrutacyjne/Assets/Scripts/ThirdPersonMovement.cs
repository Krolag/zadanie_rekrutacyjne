﻿using UnityEngine;

public class ThirdPersonMovement : MonoBehaviour
{
    #region Private Variables
    [SerializeField] private float _speed = 6f;
    [SerializeField] private float _jumpHeight = 4f;
    [SerializeField] private float _gravity = -9.81f;
    [SerializeField] private float _groundDistance = 0.5f;
    private bool isOnGround;
    private Vector3 _velocity;
    #endregion

    #region Public Variables
    public CharacterController Controller;
    public Transform Camera;
    public Transform GroundCheck;
    public LayerMask GroundMask;
    #endregion

    #region Private Methods
    private void Update()
    {
        isOnGround = Physics.CheckSphere(GroundCheck.position, _groundDistance, GroundMask);

        // Moving player
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 direction = (transform.right * horizontal) + (transform.forward * vertical);

        if (direction.magnitude >= 0.1f)
        {
            // Rotate player with camera movement
            float targetAngle = Mathf.Atan2(direction.x, direction.y) + Camera.eulerAngles.y;

            Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * direction;
            Controller.Move(moveDirection.normalized * _speed * Time.deltaTime);
        }

        // Jump
        if (Input.GetButtonDown("Jump") && isOnGround)
            _velocity.y = Mathf.Sqrt(_jumpHeight * (-2f) * _gravity);
        
        // Gravity
        _velocity.y += _gravity * Time.deltaTime;
        Controller.Move(_velocity * Time.deltaTime);
    }
    #endregion
}
