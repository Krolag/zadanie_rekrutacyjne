﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveDatabase
{
    #region Public Methods
    public static void SavePlayer(AnimationHandler variables)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string filename = Path.Combine(Application.persistentDataPath, "savefile.bin");

        FileStream stream = new FileStream(filename, FileMode.Create);

        Database database = new Database(variables);

        formatter.Serialize(stream, database);
        stream.Close();
    }

    public static Database LoadPlayer()
    {
        string filename = Path.Combine(Application.persistentDataPath, "savefile.bin");
        if (File.Exists(filename))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(filename, FileMode.Open);

            Database data = formatter.Deserialize(stream) as Database;

            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Can't find savefile");
            return null;
        }
    }
    #endregion
}